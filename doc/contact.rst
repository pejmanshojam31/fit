Contact
=======

Discovered an error? Need help? Not sure if something works as intended? Please contact us!

If you think your problem could be of general interest, please consider creating an issue on gitlab, which will then also be helpful for other users: https://gitlab.com/fitmulticell/fit/issues

If you prefer to contact us via e-mail, please write to: 

* Emad Alamoudi Emad Alamoudi (FitMultiCell) `emad.alamoudi@uni-bonn.de <emad.alamoudi@uni-bonn.de>`_
* Yannik Schaelte (rather parameter estimation) `yannik.schaelte@gmail.com <yannik.schaelte@gmail.com>`_
* Jörn Starruß (rather modeling) `joern.starruss@tu-dresden.de <joern.starruss@tu-dresden.de>`_

