Logo
====

.. image:: logo/logo_long.svg.png
   :alt: FitMultiCell logo
   :align: center

The FitMultiCell logo can be found in multiple variants in the doc/logo directory on gitlab, in svg and png format. It is made available under a `creative commons CC0 license <https://creativecommons.org/share-your-work/public-domain/cc0>`_. You are encouraged to use it e.g. in presentations and posters.
