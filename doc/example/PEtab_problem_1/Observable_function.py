import sys


def main(sumstat):
    new_sumstat = {}
    for key, val in sumstat:
        if key == "loc":
            continue
        new_sumstat["condition1_" + key] = sumstat[key]
    return new_sumstat


if __name__ == "__main__":
    sumstat = sys.argv[1]
    main(sumstat)
