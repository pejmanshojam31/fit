.. _example:

Examples
========

.. toctree::
   :maxdepth: 1

   example/minimal.ipynb
   example/Demo_CellMotility.ipynb
   example/PEtab_example.ipynb
   example/PEtab_multiple_conditions.ipynb
