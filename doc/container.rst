Containerization
================

To simplifiy and streamline the installation of morpheus, and to
enable running it on high performance infrastructure with limited
access rights, several docker images are available.

Instead of a standard system installation of Morpheus, these can be
used by replacing the ``executable`` parameter of the ``fmc.MorpheusModel``
class. By default, it is set to ``executable=morpheus``, targeting a standard
installation of morpheus in the system and accessible via the path.
Alternatively, the following container solutions can be used.
The ``MorpheusModel`` copies a reparameterized version of your model file to
``{loc}/model.xml``, where ``{loc}`` is given by the base directory specified
in the ``MorpheusModel`` plus some random subfolder to contain the model and
simulations. ``{loc}`` is a placeholder which the ``MorpheusModel`` internally
replaces by the respective folder. In the following executable expressions,
replace ``$DIR`` by the folder containing the container files.


Docker
------

* ``executable="docker run -v {loc}:/sim --rm registry.gitlab.com/morpheus.lab/morpheus/morpheus-runner dockerrun_morpheus model.xml"``


Singularity
-----------

* in ``$DIR``, run ``singularity pull -F docker://registry.gitlab.com/morpheus.lab/morpheus/morpheus-runner``
* if your ``{loc}`` is not under an automounted directory, you need to mount it. $HOME is automounted in singularity
* ``executable="singularity exec $DIR morpheus-runner_latest.sif /usr/bin/morpheus {loc}/model.xml -outdir {loc}"``


Charlie-cloud
-------------

* extract image to folder ``$DIR``: ``ch-pull2cir registry.gitlab.com/morpheus.lab/morpheus/morpheus-runner $DIR``
* note that charlie automounts your $HOME and e.g. also the /tmp directory
* ``executable="ch-run -b {loc}:/sim $DIR -- /usr/bin/morpheus -file=sim/model.xml -outdir=sim"``
