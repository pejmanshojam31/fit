.. fitmulticell documentation master file, created by
   sphinx-quickstart on Wed Oct  9 15:52:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fitmulticell's documentation!
========================================


.. image:: https://readthedocs.org/projects/fitmulticell/badge/?version=latest
   :target: https://fitmulticell.readthedocs.io/en/latest
.. image:: https://badge.fury.io/py/fitmulticell.svg
   :target: https://badge.fury.io/py/fitmulticell


Source code: https://gitlab.com/fitmulticell/fit


.. image:: logo/logo_long.svg.png
   :alt: FitMultiCell logo
   :align: center


An Integrated Platform for Data-Driven Modeling of Multi-Cellular Processes. It delivers that by combining the simulation tool `morpheus <https://morpheus.gitlab.io>`_ and the likelihood-free inference tool `pyABC <https://github.com/icb-dcm/pyabc>`_.

The overall aim of FitMultiCell is to build and validate an open platform for modelling, simulation and parameter estimation of multicellular systems, which will be utilised to mechanistically answer biomedical questions based on imaging data.



.. toctree::
   :maxdepth: 2
   :caption: User's guide

   install
   example
   container
   youtube 


.. toctree::
   :maxdepth: 2
   :caption: Developer's guide

   contribute
   deploy


.. toctree::
   :maxdepth: 2
   :caption: API reference

   api_model


.. toctree::
   :maxdepth: 2
   :caption: About

   releasenotes
   contact
   license
   logo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
