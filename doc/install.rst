Install
=======


Requirements
------------

This package requires Python 3.6 or later.
It is tested on Linux using Travis continuous integration. Other platforms
(Windows, Mac) should also be usable, but are not so thoroughly tested.


Dependencies
------------

This package relies on the tools morpheus and pyABC.

* To install morpheus, checkout `<https://morpheus.gitlab.io/#download>`_.
* To install pyabc, checkout `<https://pyabc.readthedocs.io/en/latest/installation.html>`_.


Install from PIP
----------------

The package can be installedd from the Python Package Index PyPI via pip::

   pip3 install fitmulticell


Install from GIT
----------------

If you want the bleeding edge version, install directly from gitlab::

   pip3 install git+https://gitlab.com/fitmulticell/fit

If you need to have access to the source code, you can instead download it
via::

   git clone https://gitlab.com/fitmulticell/fit

and then install from the local repository via::

   cd fit
   pip3 install -e .


Install as user into your home directory (recommended)
------------------------------------------------------

Installing fitmulticell into your system’s Python distribution can be problematic as you might not want to change your system’s Python installation or you don’t have root rights. The recommended alternative is to install fitmulticell into your home directory with::

   pip3 install --user fitmulticell


Install on AWS
--------------

Here is a link to a video that explains how to install the FitMultiCell pipeline in the AWS Amazon cloud:

https://cloudstore.zih.tu-dresden.de/index.php/s/NMiLZmpktaGGgWp

Moreover, here is the list of commands used in the video::

   sudo apt update
   sudo apt install --yes libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 \
   libxcomposite1 libasound2 libxi6 libxtst6 gnuplot-nox joe
   wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
   bash Anaconda3-2020.11-Linux-x86_64.sh
   source ~/.bashrc
   pip install jupyter
   pip install pyabc
   pip install fitmulticell
   openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mykey.key -out mycert.pem
   jupyter notebook password
   jupyter notebook --generate-config
   jupyter notebook &



Upgrade
-------

If you want to upgrade an installation, replace ``install`` by
``install --upgrade`` in the above pip commands.
