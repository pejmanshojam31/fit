.. _youtube:

Youtube tutorial series
=======

In this tutorial series, we are trying to exlain different functionality of the FitMultiCell pipeline. Here is a link to the playlist contaning all parts of the series: https://youtube.com/playlist?list=PLRgo5axBHp5jWwqSHa_XpkAJ2YnxRphCi.

Part 1
------------
The first part of this tutorial series is the explination of how to install the pipeline with its different component. To access the vedio, please follow the following link: https://youtu.be/0qVro-hkyr0.

part 2
------------

The second part is explaning the different element that need to be prepared by the user before starting the fitting process. To access the vedio, please follow the following link: https://youtu.be/Nq0yQI2gZRQ.

Part 3
----------------

The third part of the series is performing one of the application example as shown in the FitMultiCell documentaion. This example is for fitting two parameter on a cell motility model. To read more about the model, please check this jupyter-notebook: https://fitmulticell.readthedocs.io/en/latest/example/Demo_CellMotility.html. To access the vedio, please follow the following link: https://youtu.be/4tsNzU7dtdw.

Part 4
----------------

The forth part of the tutorial series is explanning how to run the FitMultiCell pipeline in large cluster suing the ditributed memory system parallelization strategy. To access the vedio, please follow the following link: https://youtu.be/w9s0aS3sDVE.
