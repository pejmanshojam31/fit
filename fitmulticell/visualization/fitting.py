import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

import fitmulticell.sumstat
import fitmulticell.util as util
import pyabc
import imageio
import copy
import matplotlib as mpl


class FittingVis:
    """
    Fitting plots for fitmulticell pipeline.
    """

    def __init__(self,
                 history: pyabc.History,
                 obs_ss: dict,
                 cell_types_list: list = None,
                 sumstat_name_separator: str = "_",
                 adjustment=None
                 ):
        """
        Parameters
        ----------
        history: pyABC.History
            A pyABC.history object
        obs_ss: dict
            A dictionary that contain the summary statistic data for the
            observed data
        cell_types_list: list
            A list of different cell types
        sumstat_name_separator: str (default = "_")
            the separator used for naming summary statistics
        adjustment: dict (optional)
            a parameter for tuning the subplot layout. e.g. adjustment =
            {"left":0.10, "bottom":0, "right":0.90, "top":1, "wspace":1,
            "hspace":0.40}

        """

        self.history = history
        self.obs_ss = obs_ss
        self.cell_types_list = cell_types_list
        self.sumstat_name_separator = sumstat_name_separator
        self.adjustment = adjustment

    def plot_fitting_cell_types(self, sumstat_func_name,
                                x_values,
                                title="fitting plot",
                                title_fontsize=16,
                                population_index=None,
                                subtitle=None,
                                subtitle_fontsize=7,
                                x_y_fontsize=12,
                                xticks=[],
                                s_color="lightsteelblue",
                                m_color="royalblue",
                                s_label="Simulation",
                                m_label="measurement",
                                x_label='x axis',
                                y_label='y axis',
                                fig_size=None,
                                set_ylimit=None,
                                linewidth=1.0,
                                measurement_marker="x"
                                ):
        """
        Fitting plot for the cell type summary statistics.

        Parameters
        ----------
        sumstat_func_name: str
            summary statistics function name
        x_values: list
           Valuse for x_coordinate, e.g. time points.
        title: str (default = "fitting plot")
           Title for the plot.
        title_fontsize: int (default = 20)
           Font size of the title.
        population_index: int
            index of population to be plotted. If nothing is giving, then the
            last population will be selected.
        subtitle: str
           sub title for subplot. If not provided, then the ss
           function name will be used.
        subtitle_fontsize: int (default = 16)
           Font size of subtitles.
        x_y_fontsize: int  (default = 12)
           The font size of x and y labels.
        xticks: list
            list of xticks, if not provided, then x_values will be used
        s_color: str
            A color for the simulated data.
        m_color: str
            A color for the measurement data.
        s_label: str
            A label name for the simulated data.
        m_label: str
            A label name for the measurement data.
        x_label: str
            A labels for x coordinate.
        y_label: str
            A labels for y coordinate.
        fig_size: Tuple[int, int]
            The size of the figure in inches
        set_ylimit: list (default = None)
            Set the y-axis view limits
        linewidth: float (default = 1.0)
            the size of the line used to plot measurement data.
        measurement_marker: str (default= "x")
            marker and linestyle for teh measurement data.

        Returns
        -------
        ax: matplotlib axis
            axis of the plot
        """
        if not population_index:
            population = self.history.get_population()
        else:
            population = self.history.get_population(population_index)
        if not subtitle:
            subtitle = sumstat_func_name
        if len(xticks) == 0:
            xticks = x_values
        if set_ylimit is not None:
            y_limits = copy.deepcopy(set_ylimit)
        else:
            y_limits = None
        particles = population._list.__len__()
        fig, ax = plt.subplots()
        fig.suptitle(title, fontsize=title_fontsize)
        if fig_size is not None:
            fig.set_size_inches(fig_size)
        y_result = []
        current_plot = 0
        first_plt_flag = True
        # y_result.append([])
        # Rearrange dataframe based on new column order.
        for j in range(particles):
            curr_particle = population._list[j].accepted_sum_stats[0]
            temp_list = []
            if not isinstance(
                    [v for k, v in curr_particle.items()
                     if sumstat_func_name in k][0],
                    pd.DataFrame):
                raise Exception("the summery statistics selected for the "
                                "simulated data doesn't seems to be for cell "
                                "type! expect pd.DataFrame but got ",
                                type([v for k, v in curr_particle.items()]))
            for q in [v for k, v in curr_particle.items()
                      if sumstat_func_name in k]:
                for k in range(len(self.cell_types_list)):
                    value = int(q.values[k])
                    temp_list.append(int(value))
            for q in range(len(self.cell_types_list)):
                y_result.append(
                    temp_list[q::len(self.cell_types_list)])

        if not isinstance(
                [v for k, v in curr_particle.items() if
                 sumstat_func_name in k][0],
                pd.DataFrame):
            raise Exception(
                "the summery statistics selected doesn't seems to be for cell"
                " type! expect pd.DataFrame but got ",
                type([v for k, v in curr_particle.items()]))
        sub_lists = util.split_list(
            y_result,
            len(self.cell_types_list))
        for sub_sub_list, cell_type_index in zip(
                sub_lists, self.cell_types_list):
            first_plt_flag = True
            for sub_sub_sub_list in sub_sub_list:
                if first_plt_flag:
                    current_plot += 1
                    ax = plt.subplot(
                        len(
                            self.cell_types_list), 1, current_plot)
                    ax.plot(
                        x_values,
                        sub_sub_sub_list,
                        c=s_color,
                        label=s_label + sumstat_func_name
                        + str(cell_type_index))
                    first_plt_flag = False
                    ax.set_ylabel(
                        y_label, fontsize=x_y_fontsize)
                    ax.set_title(subtitle, fontsize=subtitle_fontsize)
                    if y_limits is not None:
                        ax.set_ylim(y_limits.pop(0))
                    ax.legend()
                else:
                    ax.plot(
                        x_values, sub_sub_sub_list, c=s_color,
                        label='_')
                    ax.set_xticks([])
        ax.legend()

        if xticks.all:
            ax.set_xticks(xticks)
        else:
            ax.set_xticks(x_values)

        ax.set_xlabel(x_label, fontsize=x_y_fontsize)
        if self.adjustment:
            plt.subplots_adjust(left=self.adjustment["left"],
                                bottom=self.adjustment["bottom"],
                                right=self.adjustment["right"],
                                top=self.adjustment["top"],
                                wspace=self.adjustment["wspace"],
                                hspace=self.adjustment["hspace"])
        # plot the measurement data
        curr_plt = 0
        obs_list = [v for k, v in self.obs_ss.items()
                    if sumstat_func_name in k]
        obs_temp_list = []
        if not isinstance(obs_list[0], pd.DataFrame):
            raise Exception(
                "the summery statistics selected for the measurment data "
                "doesn't seems to be for cell type! expect pd.DataFrame "
                "but got ",
                type(obs_list[0]))

        for obs_item in obs_list:
            for k in self.cell_types_list:
                obs_temp_list.append(obs_item["n_cells"])
        for q, cell_type_index in zip(
                range(len(self.cell_types_list)),
                self.cell_types_list):
            curr_plt += 1
            ax = plt.subplot(len(self.cell_types_list), 1,
                             curr_plt)
            ax.plot(x_values,
                    obs_temp_list[q::len(self.cell_types_list)],
                    measurement_marker,
                    c=m_color, linewidth=linewidth,
                    label=m_label + ": " + sumstat_func_name + "_" + str(
                        cell_type_index))

            ax.legend()
        return ax, fig

    def plot_fitting_cluster(self, sumstat_func_name,
                             x_values,
                             title="fitting plot",
                             title_fontsize=16,
                             population_index=None,
                             subtitle=None,
                             subtitle_fontsize=7,
                             x_y_fontsize=12,
                             xticks=[],
                             s_color="lightsteelblue",
                             m_color="royalblue",
                             s_label="Simulation",
                             m_label="measurement",
                             x_label='x axis',
                             y_label='y axis',
                             fig_size=None,
                             set_ylimit=None,
                             linewidth=1.0,
                             measurement_marker="x"
                             ):
        """
        Fitting plot for the cluster count statistics.
        Parameters
        ----------
        sumstat_func_name: str
            summary statistics function name
        x_values: list
           Valuse for x_coordinate, e.g. time points.
        title: str (default = "fitting plot")
           Title for the plot.
        title_fontsize: int (default = 20)
           Font size of the title.
        population_index: int
            index of population to be plotted. If nothing is giving, then the
            last population will be selected.
        subtitle: str
           sub title for subplot. If not provided, then the ss
           function name will be used.
        subtitle_fontsize: int (default = 16)
           Font size of subtitles.
        x_y_fontsize: int  (default = 12)
           The font size of x and y labels.
        xticks: list
            list of xticks, if not provided, then x_values will be used
        s_color: str
            A color for the simulated data.
        m_color: str
            A color for the measurement data.
        s_label: str
            A label name for the simulated data.
        m_label: str
            A label name for the measurement data.
        x_label: str
            A labels for x coordinate.
        y_label: str
            A labels for y coordinate.
        fig_size: Tuple[int, int]
            The size of the figure in inches
        set_ylimit: list (default = None)
            Set the y-axis view limits
        linewidth: float (default = 1.0)
            the size of the line used to plot measurement data.
        measurement_marker: str (default= "x")
            marker and linestyle for teh measurement data.

        Returns
        -------
        ax: matplotlib axis
            axis of the plot.
        """
        if not population_index:
            population = self.history.get_population()
        else:
            population = self.history.get_population(population_index)
        if not subtitle:
            subtitle = sumstat_func_name
        if len(xticks) == 0:
            xticks = x_values
        particles = population._list.__len__()
        fig, ax = plt.subplots()
        fig.suptitle(title, fontsize=title_fontsize)
        if fig_size is not None:
            fig.set_size_inches(fig_size)
        y_result = []
        current_plot = 1
        first_plt_flag = True
        # Rearrange dataframe based on new column order.
        for j in range(particles):
            curr_particle = population._list[j].accepted_sum_stats[0]
            if not isinstance(
                    [v for k, v in curr_particle.items()
                     if sumstat_func_name in k][0],
                    int):
                raise Exception(
                    "the summery statistics selected for the simulated data "
                    "doesn't seems to be for cell type! expect int but got ",
                    type([v for k, v in curr_particle.items()]))
            y_result.append([v for k, v in curr_particle.items()
                             if sumstat_func_name in k])
        if not isinstance(
                [v for k, v in curr_particle.items()
                 if sumstat_func_name in k][0],
                int):
            raise Exception(
                "the summery statistics selected for the simulated data "
                "doesn't seems to be for cell type! expect int but got ",
                type([v for k, v in curr_particle.items()]))
        for item in y_result:

            if first_plt_flag:
                current_plot += 1
                ax = plt.subplot()
                ax.plot(x_values, item, c=s_color, label=s_label)
                first_plt_flag = False
                ax.set_ylabel(
                    y_label, fontsize=x_y_fontsize)
                ax.set_title(subtitle, fontsize=subtitle_fontsize)
                if set_ylimit is not None:
                    ax.set_ylim(set_ylimit)

            else:
                ax.plot(x_values, item, c=s_color, label='_')
                ax.set_xticks([])
            ax.legend()
        if xticks.all:
            ax.set_xticks(xticks)
        else:
            ax.set_xticks(x_values)

        ax.set_xlabel(x_label, fontsize=x_y_fontsize)
        if self.adjustment:
            plt.subplots_adjust(left=self.adjustment["left"],
                                bottom=self.adjustment["bottom"],
                                right=self.adjustment["right"],
                                top=self.adjustment["top"],
                                wspace=self.adjustment["wspace"],
                                hspace=self.adjustment["hspace"])
        # plot the measurement data
        obs_list = [v for k, v in self.obs_ss.items()
                    if sumstat_func_name in k]
        if not isinstance(obs_list[0], int):
            raise Exception(
                "the summery statistics selected for the measurment data "
                "doesn't seems to be for cell type! expect int but got ",
                type(obs_list[0]))

        ax.plot(
            x_values,
            obs_list, measurement_marker,
            c=m_color,
            label=m_label,
            linewidth=linewidth)
        ax.set_xticks([])
        ax.legend()
        return ax, fig

    def plot_fitting(self, sumstat_func_name,
                     x_values,
                     title="fitting plot",
                     title_fontsize=16,
                     population_index=None,
                     subtitle=None,
                     subtitle_fontsize=10,
                     x_y_fontsize=12,
                     xticks=[],
                     show_measurement=True,
                     s_color="lightsteelblue",
                     m_color="royalblue",
                     s_label="Simulation",
                     m_label="measurement",
                     x_label='x axis',
                     y_label='y axis',
                     fig_size=None,
                     set_ylimit=None,
                     linewidth=1.0,
                     measurement_marker="x",
                     group_by=1,
                     ax: mpl.axes.Axes = None,
                     fig=None,
                     alpha=1,
                     clean=True
                     ):
        """
        Fitting plot for the cluster count statistics.
        Parameters
        ----------
        sumstat_func_name: str
            summary statistics function name
        x_values: list
           Valuse for x_coordinate, e.g. time points.
        title: str (default = "fitting plot")
           Title for the plot.
        title_fontsize: int (default = 20)
           Font size of the title.
        population_index: int
            index of population to be plotted. If nothing is giving, then the
            last population will be selected.
        subtitle: str
           sub title for subplot. If not provided, then the ss
           function name will be used.
        subtitle_fontsize: int (default = 16)
           Font size of subtitles.
        x_y_fontsize: int  (default = 12)
           The font size of x and y labels.
        xticks: list
            list of xticks, if not provided, then x_values will be used
        show_measurement: bool (default = True)
            A flag to show the measurement data.
        s_color: str
            A color for the simulated data.
        m_color: str
            A color for the measurement data.
        s_label: str
            A label name for the simulated data.
        m_label: str
            A label name for the measurement data.
        x_label: str
            A labels for x coordinate.
        y_label: str
            A labels for y coordinate.
        fig_size: Tuple[int, int]
            The size of the figure in inches
        set_ylimit: list (default = None)
            Set the y-axis view limits
        linewidth: float (default = 1.0)
            the size of the line used to plot measurement data.
        measurement_marker: str (default= "x")
            marker and linestyle for teh measurement data.
        group_by: int (default= 1)
            grouping multiple points together.
        ax:
            The axis object to use. A new one is created if None.
        fig:
            Figure

        Returns
        -------
        ax: matplotlib axis
            axis of the plot.
        fig:
            Figure
        """
        if not population_index:
            population = self.history.get_population()
        else:
            population = self.history.get_population(population_index)
        if len(xticks) == 0:
            xticks = x_values
        particles = population._list.__len__()
        if ax is None:
            if fig_size is None:
                fig, ax = plt.subplots()
            else:
                fig, ax = plt.subplots(figsize=fig_size)
        nr_rows = self.obs_ss.shape[1]
        fig.suptitle(title, fontsize=title_fontsize)
        for j in range(particles):
            curr_particle = population._list[j].sum_stat
            # TODO: remove next 3 lines, with the clean argument
            if not clean:
                unique_obs = [0, 8, 15, 20, 30, 50]
                curr_particle = fitmulticell.model.base.\
                    prepare_subset_of_sumstat(curr_particle, unique_obs, 10)

            if not isinstance(
                    [v for k, v in curr_particle.items() if
                     sumstat_func_name in k][0],
                    int):
                q = [v for k, v in curr_particle.items() if
                     sumstat_func_name in k]
                for [i, v], index in zip(enumerate(range(nr_rows)),
                                         range(0, len(q[0]), group_by)):
                    v = v + 1
                    simulation_result = q[0][index:index+group_by]
                    ax1 = plt.subplot(nr_rows, 1, v)
                    # to adapt between direct sumstat and explicit ones
                    if len(simulation_result) == 1:
                        simulation_result = simulation_result[0]
                    if j == 0:
                        ax1.set_xticks(xticks)
                        ax1.set_ylim(set_ylimit)
                        ax1.set_ylabel(y_label, fontsize=x_y_fontsize)
                        ax1.set_title(subtitle[v-1],
                                      fontsize=subtitle_fontsize)
                        ax1.plot(x_values, simulation_result, c=s_color,
                                 label=s_label, alpha=alpha,
                                 linewidth=linewidth)
                    if j != 0:
                        ax1.plot(x_values, simulation_result, c=s_color,
                                 alpha=alpha, linewidth=linewidth)
                    if j == particles-1 and show_measurement:
                        ax1.plot(x_values, self.obs_ss[:, i],
                                 measurement_marker,
                                 label=m_label, c=m_color, linewidth=linewidth)
                        if nr_rows == v:
                            ax1.set_xlabel(x_label, fontsize=x_y_fontsize)
        if self.adjustment:
            plt.subplots_adjust(left=self.adjustment["left"],
                                bottom=self.adjustment["bottom"],
                                right=self.adjustment["right"],
                                top=self.adjustment["top"],
                                wspace=self.adjustment["wspace"],
                                hspace=self.adjustment["hspace"])

        ax1.legend()
        # fig.tight_layout()

        return ax, fig

    def plot_multi_fitting(self,
                           func,
                           sumstat_func_name,
                           x_values,
                           subtitle=None,
                           title_fontsize=16,
                           subtitle_fontsize=7,
                           x_y_fontsize=12,
                           xticks=[],
                           s_color="lightsteelblue",
                           m_color="royalblue",
                           s_label="Simulation",
                           m_label="measurement",
                           x_label='x axis',
                           y_label='y axis',
                           fig_size=None,
                           set_ylimit=None,
                           title=[],
                           linewidth=1.0,
                           measurement_marker="x",
                           group_by=1,
                           file_name="fitting_plot",
                           fps=1.0,
                           ):
        """
        create a gif of the fitting plot for all populations in a pyABC.history

        Parameters
        ----------
        func: str
            function name that you want to plot as gif
        sumstat_func_name: str
            summary statistics function name
        x_values: list
           Valuse for x_coordinate, e.g. time points.
        title: str (default = "population t")
           Title for the plot.
        title_fontsize: int (default = 20)
           Font size of the title.
        subtitle: str
           sub title for subplot. If not provided, then the ss
           function name will be used.
        subtitle_fontsize: int (default = 16)
           Font size of subtitles.
        x_y_fontsize: int  (default = 12)
           The font size of x and y labels.
        xticks: list
            list of xticks, if not provided, then x_values will be used
        s_color: str
            A color for the simulated data.
        m_color: str
            A color for the measurement data.
        s_label: str
            A label name for the simulated data.
        m_label: str
            A label name for the measurement data.
        x_label: str
            A labels for x coordinate.
        y_label: str
            A labels for y coordinate.
        fig_size: Tuple[int, int]
            The size of the figure in inches
        set_ylimit: list (default = None)
            Set the y-axis view limits
        linewidth: float (default = 1.0)
            the size of the line used to plot measurement data.
        measurement_marker: str (default= "x")
            marker and linestyle for teh measurement data.
        file_name: str (default="fitting_plot")
            name for the output gif file.
        fps: float (default=1.0)
            number of frames per second.

        Returns
        -------
        ax: matplotlib axis
            axis of the plot
        """
        matplotlib.use('Agg')
        history_nr = self.history.get_nr_particles_per_population()
        images_list = []
        if len(title) == 0:
            for i in range(history_nr.__len__() - 1):
                title.append("population " + str(i))
        for i in range(history_nr.__len__() - 1):
            ax, fig = eval("self."+func)(sumstat_func_name,
                                         title=title[i],
                                         title_fontsize=title_fontsize,
                                         population_index=i,
                                         subtitle=subtitle,
                                         subtitle_fontsize=subtitle_fontsize,
                                         x_values=x_values,
                                         x_y_fontsize=x_y_fontsize,
                                         xticks=xticks,
                                         s_color=s_color, m_color=m_color,
                                         s_label=s_label, m_label=m_label,
                                         x_label=x_label, y_label=y_label,
                                         fig_size=fig_size,
                                         set_ylimit=set_ylimit,
                                         linewidth=linewidth,
                                         measurement_marker=measurement_marker,
                                         group_by=group_by)
            fig.canvas.draw()  # draw the canvas, cache the renderer
            image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
            image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
            images_list.append(image)
        imageio.mimsave('./' + file_name + '.gif', images_list, fps=fps)
